# Gitlab Open In Editor

Add buttons to GitLab files/diffs to open them in your local editor. Currently supports VSCode and Emacs.  
Get it from the [Chrome Web Store](https://chrome.google.com/webstore/detail/gitlab-open-in-editor/gimnckknlihboacimdhckfidphnaihon?hl=en&authuser=0)
	
## What Does it Do?

Adding this extension creates icons in Gitlab next to the built-in "Copy File Path" icon that, when clicked, will open that file in your editor.

For any project in Gitlab, you specify the location on your computer where that project has been cloned. This extension will add a link for Visual Studio Code and/or Emacs to open the file shown in a diff in your editor at the line displayed.

## How Do I Use It?

After installing the extension, navigate to any page in your Gitlab project. Click the <img height="16px" width="16px" src="icon16.png" style="display: inline-block"> icon in your browser toolbar. The "Gitlab Project" field will be populated with your project's base URL. Fill in the "Local Path" field with the local path where you cloned your project. Select which editor you want to create links for. Click "Save".

When you navigate to (or reload) a page in Gitlab with the "Copy File Path" icon, you'll see icons for your editor(s) as well. Click the icons to open the file in your editor at the line displayed.

## Screenshots

### Configuration
<img src="config-ui.png" style="border: 2px solid black;">

### Links in Gitlab Diffs
![](diff-links.png)

## Requirements

Visual Studio Code will work out of the box. Emacs requires the installation of [Emacs URL Handler](https://github.com/typester/emacs-handler) on macOS.

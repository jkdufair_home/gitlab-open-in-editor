// TODO: Handle adding from the project main page (i.e. no .../-/...)

/*
 * Local storage:
 * {
 *   projects: { 'https://gitlab.com/team/group/project-name': '/Users/foo/projects/project-name' },
 *   editors: ['emacs']
 * }
 */

const availableEditors = ['emacs', 'vscode', 'rider', 'pycharm']

/*
 * Iterate over all project rows and construct the `projects` key.  Iterate over the available
 * editors and construct the `editors` key according to whether the box is checked.
 */
const savePreferences = () => {
	const projects = {}
	document.querySelectorAll('tr').forEach(row => {
		const elements = row.querySelectorAll('input')
		if (elements[0]) {
			projects[elements[0].value] = { localPath: elements[1].value, projectName: elements[2].value }
		}
	})

	const editors = availableEditors.reduce((result, editor) => {
		document.getElementById(editor).checked && result.push(editor)
		return result
	}, [])

	chrome.storage.local.set({ editors, projects }, () => {
		// Briefly show saved message
		const savedDiv = document.getElementById('saved')
		savedDiv.style.display = "inline-block"
		setTimeout(() => {
			savedDiv.style.display = "none"
		}, 3000)
	})
}

/*
 * Remove the project row from the DOM whose delete button was clicked
 */
const removeProject = event => {
	event.target.parentElement.parentElement.remove()
}

/*
 * Add a new project row to the DOM, constructing the delete button to call the remove method.
 */
const addProject = (gitlabProject, {localPath, projectName}) => {
	const newProjectRow = document.createElement('tr')
	newProjectRow.className = 'project-row'
	newProjectRow.innerHTML = `
<td class="gitlab-project" >
<input type="text" value=${gitlabProject}></input>
</td>
<td class="local-path">
<input type="text" value=${localPath ?? ''}></input>
</td>
<td class="project-name">
<input type="text" value=${projectName ?? ''}></input>`
	const deleteButtonTd = document.createElement('td')
	const button = document.createElement('button')
	button.innerText = '-'
	button.onclick = removeProject
	deleteButtonTd.appendChild(button)
	newProjectRow.append(deleteButtonTd)
	document.querySelector("#projects>tbody").append(newProjectRow)
}

/*
 * Use a regex to determine the base part of the Gitlab url that should be the same across MRs,
 * diffs, etc. Then call the method to add the project to the DOM (because async)
 * TODO: handle self-hosted
 */
const getGitlabProjectBaseUrl = () => {
	chrome.tabs.query({active: true, currentWindow: true}, tabs => {
		const url = tabs[0]?.url
		// Gitlab project features are all prefixed with "/-/" in the project page prior to the feature
		const projectPath = url.split("/-/")[0]

		let lastLocalPath = ''
		let lastProjectName = ''
		const projectRows = document.querySelectorAll('.project-row')
		if (projectRows.length > 0) {
			const lastProjectRow = projectRows[projectRows.length - 1]
			lastLocalPath = lastProjectRow.querySelector('.local-path input').value
			lastProjectName = lastProjectRow.querySelector('.project-name input').value
		}

		addProject(projectPath, {localPath: lastLocalPath, projectName: lastProjectName})
	})
}

window.onload = () => {
	document.getElementById('save-preferences').onclick = savePreferences
	document.getElementById('add-project').onclick = () => getGitlabProjectBaseUrl()

	// Get prefs from local storage & populate/update the DOM
	chrome.storage.local.get(['editors', 'projects'], storedValue => {
		console.log(storedValue)
		if (Object.keys(storedValue.projects)) {
			Object.keys(storedValue.projects).map(gitlabProject => {
				addProject(gitlabProject, storedValue.projects[gitlabProject])
			})
		}
		availableEditors.map(editor => {
			storedValue.editors.includes(editor) && (document.getElementById(editor).checked = true)
		})

		chrome.tabs.query({active: true, currentWindow: true}, tabs => {
			const url = tabs[0]?.url
			// Gitlab project features are all prefixed with "/-/" in the project page prior to the feature
			const projectPath = url.split("/-/")[0]
			const projectRows = document.querySelectorAll('.project-row')
			let addButtonEnabled = true
			projectRows.forEach(projectRow => {
				if (projectRow.querySelector('.gitlab-project').value === projectPath) {
					addButtonEnabled = false
					projectRow.classList.add('highlight-row')
				}
			})
			if (addButtonEnabled) {
				document.getElementById('add-project').removeAttribute('disabled')
			}
		})
	})
}

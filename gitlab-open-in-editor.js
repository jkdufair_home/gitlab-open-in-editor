const emacsIcon = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAABGdBTUEAALGPC/xhBQAAAHhlWElmTU0AKgAAAAgABQESAAMAAAABAAEAAAEaAAUAAAABAAAASgEbAAUAAAABAAAAUgEoAAMAAAABAAIAAIdpAAQAAAABAAAAWgAAAAAAAABIAAAAAQAAAEgAAAABAAKgAgAEAAAAAQAAABigAwAEAAAAAQAAABgAAAAAXseQHAAAAAlwSFlzAAALEwAACxMBAJqcGAAAAVlpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IlhNUCBDb3JlIDUuNC4wIj4KICAgPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6dGlmZj0iaHR0cDovL25zLmFkb2JlLmNvbS90aWZmLzEuMC8iPgogICAgICAgICA8dGlmZjpPcmllbnRhdGlvbj4xPC90aWZmOk9yaWVudGF0aW9uPgogICAgICA8L3JkZjpEZXNjcmlwdGlvbj4KICAgPC9yZGY6UkRGPgo8L3g6eG1wbWV0YT4KTMInWQAABvFJREFUSA1tVWlsXFcV/u677828Gc/Ynhlvset4XxKb2HFMVbZgVBF1SVOQSARq4QdIofCjAiml8KPIRihQodA/FNRKRY2LqBojlba0Km2cOE0wSmrXWbDdxHY9Hi9jezbP+vb3uHccR2nhat68d8899yzf+c69BJ8ZjuNQJrIJIQ5fcpxCHSB1wxJ3g8ADATkmXmTPDaYT39Yp7nHY3Obzu4d492RiwpGYksFlpqkeMQvuJxJr9leILfhsE7DYw4fNXQtmIpswR31B5w9sz0UuZ8GJ7Pu2FpeAxXR7TE9Pu7q6uvRIJFIb9O96Wc+JX19ZAJYW4kjG83aollrVDR7H6xOJLEtUlj2CmxJwX26/8Zdb6j9/2F/7SGFiYkLq7+8vBslNFx3wyPv7iTE3F+4LldSNptbF8oWFDbOijrKUbaoWTEFTLGIYNmzLAZWIU1XrsTr2Be103BIdxS1Qrza/Gvt4oLe3d5VnMjQ0tB36+fPnizCdfftyezSsFCZHVefN4UVV102bKf7PsG3b0TTTyef0nTV7LZJXPr7sONFwfvGxxwZLty1v/+9ARG5dj1+HEeq+8N6MVtfsdj94rIVhCqiKganxdRRyDFqmLUoC/OUSygJulPj5W0YqruD9v62qhw7vlbcK0Xe6+moffvn1146txDakYvTvjkw95ZFC3ePjc5oome7ZiTxaukrR3lUJQSBYmstg4u8pBBtdzAGBpjDkmHe5VIAvIDBC2PD6qXz53/NWV0/dQy+98O6v5qPXnlmNxF/jlPQ8fP/xYULkskQsBUM3Bb755lQKoTo3qnb58LnPV6GmU8L6Sh6GZiO4ywVPmQAqEmbcYjVhDonFTNkCFagZc8Jfm1uZwdJs+Hv0Wz1/emB9SfnR4vyalYgqtJCxCIdGV218dC4GzVIRqvagsTWIPX1BKLqKhasZEA6Xm7HIsZDeysJyNMTXMphdn7Iy8gKNR1N/fePPrzwvyuXk4NZNA/GZvHXkFy20594GVNWUQRQFFhFBNqNhbTkLVluEKktw+NsdaOvexPsjYcQiCkqrBKwvJxFbSkJ1FKftO2miZUSsjPnGmAGIiWmls/P+Cvz02UOkvqGSyz41qmv9n5rzSUd3FZragrhyMYIr51ag5BVko3lUf1MhrhLqhMddCK42dXFd8VI8Uz70ZA+4cY4n60RQygpnWNhcz2IzmmMGzGIGFusBU7eg63ax2JKL1cFtIBvLofVBEaWdFNHVtGNNV4Pu1suxwRx4YRtl5V7uDI7NNrokfDIXw9g/PsHavAJT4716h81F7IngMDLYkNxAcisGyAoae8vRvKcBw+9cJbLtgk6yxW4WN6Cu3ZhaQvveWoeKFFupHF76zSRcLhGBGpl1Lvdc/G2/uT8mIITCMDWsX40j0Amsmcu49vo8hPA9QMBggVmrXFP4Rp9/8sXHP8T1KbYoMF77ZTz6/TaIsoMYo2VuS4OuMYgsflDazDA7LphXBxYrfgyGk4a/2YBasJG4XOoQmVDDYplb+hR3QHs6DuZM2f7x6OkF2tBbYtc3VpL6xgrsu7cG/hCrhcUwTmnIJDTk0zozZEJg7amoeUbXMKr6CVw1Clan2BkVCTqC3xZ0tZBQjcLPFuOXFDodHtvc2/TVL8glvtbTv71hqDRJQ1UluGd3JRqaK7D/vnrs/2ItOvpCqO/wMdhc0HQF42/PItgkINClIh7LIn0uCLGMHfWs1zQrP/zezK9HDhw4LvFORo33wAJjxA9qW2Xyr+EN59Xn/0PWEisMY71YVLcsIRAqQV19AM3tFWho8+PgIy2QghpmN6aRvR6AnZQtuCxJ03OKoiaPhZNXMl+ONhM6MDAoXvjoZKSp6j63lZYP9j4a0A893kh9pS4S20hjORxHdCV5+0kgsrTOMtDA1nFp5gOoMQ+0WyW2yuhGWREVM/3EuZvPXeDRn42+YtJweKzIi/mNi6O7K/valaS4P1/IWW4vtctDMikNuInXxxJlZ43KoFFVhvdSHKubEURTG87UadWEwzpHEmhOSZwcnf3dqaNHz9CzZ39evNmKBB/EoMAeThMMtJ54zlrx/CSnEmzBZJ1BLML6gDKwBMYiL/u6xlZ++UzQMfLl4oXfZ0iw30QulT8xtnDqFKPw7abZvtN3OghHcZSOYISzHoe/9NRDMMWTEvX2iMRdvBccx2bUdKAlgbI9WxA9Bcy+6kGwj3yQjWVPjC//8cNtG2f40VpEhdu644BPuPeBgSE6NjZYTG+g5ckHBNF1hN0JfUQQdrm8VHJ7BLuuy8jdnMi+paf9b4zdevYS38kxn5x8ke+7Y5zL/+/gyp9dOLTvuyVPH3+67MyZF8o2Nzd9O+ssKxbUYPHi2pHd/f4vLIh0tLODnu0AAAAASUVORK5CYII='

const vscodeIcon = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAeGVYSWZNTQAqAAAACAAFARIAAwAAAAEAAQAAARoABQAAAAEAAABKARsABQAAAAEAAABSASgAAwAAAAEAAgAAh2kABAAAAAEAAABaAAAAAAAAAEgAAAABAAAASAAAAAEAAqACAAQAAAABAAAAGKADAAQAAAABAAAAGAAAAABex5AcAAAACXBIWXMAAAsTAAALEwEAmpwYAAABWWlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iWE1QIENvcmUgNS40LjAiPgogICA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPgogICAgICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIgogICAgICAgICAgICB4bWxuczp0aWZmPSJodHRwOi8vbnMuYWRvYmUuY29tL3RpZmYvMS4wLyI+CiAgICAgICAgIDx0aWZmOk9yaWVudGF0aW9uPjE8L3RpZmY6T3JpZW50YXRpb24+CiAgICAgIDwvcmRmOkRlc2NyaXB0aW9uPgogICA8L3JkZjpSREY+CjwveDp4bXBtZXRhPgpMwidZAAAFw0lEQVRIDZ1VWWxUVRj+z7n33DtbZ+lCbYuGpSy1LDE1IgWxJPKAIDEhrTwaHzAx4lZANFFHG0hEDKEPJjwo8cGgbSIPLmUH0RRMLFqTUQmVkAoWtNv0Tqd3OYv/mZmiQojBk9x758495//+//vO9x8C/2ekFYU3QAEhatWunh3hVPWL8XvmH+cSdh96LNavQ7Z2KUM/ib7dyWjt6jK629qEXrNi98lOFopuUVMTUNP0CMjAB8n9Q1LJ3Z9uSJzTc+4IoCV9yjydXs31wgd3HT9olSU3Bc5oYICklQ3LJDGZaUWjhHsIFPjHpAzaqZ4MoMh0ScX3W+9Nm/ezQnClyLJdx48a0fgm1xn3hVQML0MoYBJJcydyAQ98zpKxNYqwHgJYMpRK1iDdrSA1t/+EaEx3WZl0m48VxCaZOG2EY018yvFwjq2kAKxAVjUuJ9RgBBQuxTtG4AII/oFDlz5WOWNB/7ONGf1eAGojBZ6ngzftPFyjBP2GhqNzhJvzMYql54IU3DSoiRQBMZgCJQsxCd6FRKD6zot2hPhHDTu2Skzl9vz4fOM2va5p/3fMHbpEdOaLOo41GFKcoXakUnh5nxBiYaIKn5xaIQauM1i95KEkoWZcSiQL0TUKUgZk8b7MeRqO3yfyWW7Gyk0+OdqrPLIxs73xmgZalD7SjBqdpKZl4w4pZa600IYZSZAgN/pJvKa+vay6rg8pqsY5Er8VtJWYhYkCEYVQiCuEMyqR32YpnYGFe39ebwyeC+GkHqAmCM8LMC+kRfoIhhVI8HPZrT+9te7dllMqJkYmbEkwri4NEaYrMPPZP5fbCk7QSKJZ5rO+zDsBMe0oineKl88ByA1jrozjxZBfn9oxi7u5IaRn4y8da89iLDAGHVtGQBJV1Fj/p4emiF5Or3YvvLRoBc+NfwHRlIWVUB54QvAARLQKRGKmFIFvIvcAoYQVTDk9phmer4PXd35p60AiXiYEBtcBtQL6OX1RKFn64tbF6wNn7D3FIrinkS8gClGUsGNUVNUrYdgQDF9+YWDnukcz6dU57YuBLWtREwCez5JicHULiAnd3XpOYQSD/cqomg0ynAQiA1LwA0c9UQMon4X0Ymql0Ve7GW01/VbMnJYy1/9Oa3Bjysztn31uhMvWibGrEmIzFKRqKPoHRdOaYXPDYZRVgHBGelziPDH88kpHb/GB5+Z5LYfGkq5r/Io+KJc8QKMCvQEw68kDIa+y/ARu1WblOj6mSYB7TIXiAKk6LAKFk2hKRQxc5JNw3FJTzhBXsPGPV5cURH7gwysVisUvEMOsUBx7KiakjaBpo/mK1FllRZt5fsIVmLFkESaolVPXL7VgBWvRjbg9mNYlQAEt5NsXBqvBYL0zdv7QriuLhOs8XFvSTutXpEwLTjGmQi+grZUp7TJLuJO90mDzrh145quhV5YeVjxYIYLAU4atm5qPu8USvscF9xVEUnvKO/o+/n3kSkIR6uEGLLJa0kJLRgApSiWThwkLPwy++87ovse366wAdwnUriSQbvQrOvoaJNAzxIpUKj8/7WZ94HBiRRnNDf9WV9+QQG/H0T8YFr/gTVOknwDY7MqzzoLRvRsKzQ7SaROvQt+HdMbSIJH0+VpGyNcQis1R2OwwQqHZYe8JmBLsrtnzgRoGGhw1xKERtKkxQ9whaaL7h26jBnS34u9/t2vAxgdP3x9UpTMxl3in0XBNairrYxQLzYL9xpM1cxf+3a4LwSBA2ortGrEItHZT6C4ehcXvN91LIHqXRd78/giEk2tUftxXUjImXFIztwHtgu1aYiNEBxnxuMmz2atFim6KddtXpBJKR2botb6DKprapJyRwBQura2/F49M06ShOHrUBQi8Y4LI9jsD0Mi6tZQOI/b6+U7JoluMyWG4e/Ey7Bl4yOGhLwh9+/JTiW9vm+h/ftC6aUPqsa13R7Kj//qsD8Y/mv3+2NIba3UieP0FHDMasgFPoGkAAAAASUVORK5CYII='

const riderIcon = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAAXNSR0IArs4c6QAAAIRlWElmTU0AKgAAAAgABQESAAMAAAABAAEAAAEaAAUAAAABAAAASgEbAAUAAAABAAAAUgEoAAMAAAABAAIAAIdpAAQAAAABAAAAWgAAAAAAAABIAAAAAQAAAEgAAAABAAOgAQADAAAAAQABAACgAgAEAAAAAQAAABigAwAEAAAAAQAAABgAAAAAEQ8YrgAAAAlwSFlzAAALEwAACxMBAJqcGAAAAVlpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IlhNUCBDb3JlIDYuMC4wIj4KICAgPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6dGlmZj0iaHR0cDovL25zLmFkb2JlLmNvbS90aWZmLzEuMC8iPgogICAgICAgICA8dGlmZjpPcmllbnRhdGlvbj4xPC90aWZmOk9yaWVudGF0aW9uPgogICAgICA8L3JkZjpEZXNjcmlwdGlvbj4KICAgPC9yZGY6UkRGPgo8L3g6eG1wbWV0YT4KGV7hBwAABpVJREFUSA1tVn1slVcZ/52P933v90dvafkoV0oGq+WjuKCkYGRjUAcOs2wjC9G6dkrFpMtc6sJwai7JDH+4kBSsE0J1mjqUjRhhG4EVJnG64hChWykrH/WjpQXWdm1ve+99v47P+9Z2TnaSc849z3nO8zvP7znP817gv00B7BwatOn1WdTt7ax++OJg/ZJ10zLSkZmM0r11/Nn8Qv2Hapm/l1Hcnz9lkJ7sLWQkQ8YGDljvxGpXCcFeitpahXUtDqsoferqxpKjapjvZGdPXUKG4SvPquQJ6ZwTrnmajj9K3QNwqd/RuHfr+3zjwNn4N35sMNGhuaJiVOat7JBjD7yXcIKu9lVLD3dd+Qz2HNjy+xWvs/xfi5lIJiZH16Ph0hwCtbFxr0HW7/CEeZDvxL9+j4B4Kcb0ZVmYSirhwFISQsEd5zAq81YkPqhlI/dh19LtOCxCqMp9lDNEOBgsjDWd2Z/e8z9X/4Q3siNW+yMNYpdkHBMwzUEzq13RRmQoHcEUswxKQQsMDatXIinnjb4/8TX2KGwFw6CgFMaHn/KN80hyNpftg/ZHZ2j9MQjRMnQhUa8IqHAxVq9ejNfQMXx6D9wh93hX8+cvmNb/iQf2JO7y6PKbZAx/keAPKgauQhxsIIeVD6zHC8/sRDAUgpQSQ0ND2N/SgiPHjmH/gVYsX7YUXJDHExPsyKtHVEvLvslZpaXh0ptZ633iYR+uFrzYvoa5DjoStc2XE9tUR7zWvLhgm9qHKvXwtnrltZGREXW9t9f/ffv2bf+WXV1d/rqnp8ff9xa/OHjQ39t718ZcV/CJFw4nt8SnPeBCsUuKPCcNBlfRIJHLF/z9HTt2YGF5OZqbm1FcXIyamhrk83kMDAxg8eLFSCaTuNjZibq6eqBivnKvjgfCyUBT2glcezfx+NMnSmvD3GHoMeGAqOJMuMSUBY1o8SLb1NSE48ePY+vWrRgbG8PJkycpsgY0bSYfcejQITCiq2HzFjaBTjUWtk2mWKqYBffE83iNq4DZawUKBS1scxmwlYAJlzwhzxGJRLB27VqUlJRg+/btvleccxQKUx56gvvvX+fL3+q8AIEKhpyru1BmVlnEBm/jsZRzQ4atgUDEhgybBDDpMQVGhlpbW5EqKvINNDY2+jMjV+fNm4f29nZ0d3djw/oNaD/5Jq6cOI3xyruRm5R2hAl9Utnvrhr9VStf0vWKKSLmPzwAbhSUxg30jQzj+tVr8G6bI87b2tpQVlaGcooHBdmPQWVlJUL0yrxLbPhyjQ/enwuowVQJl5YNR8qdvtAbuqs3/bz/3gdV77oHzN/e83n/RZDYn9Pp9MyaqJr5Pb3vzWWzpnQeW9Rotaz+tToz61u/IzkOU877xU5ECt3SkHAEeZGb4rf6cyswkstTbpuoqqry9NHX14d4PI5oNOqvp4P9Yf+EvzY1Q/XHS3F++YYLOHUQl5ZgCkCLmZddMi6Fy2XYuyTVp79f8Of/H1KplA/k0ec9BK8H5GxfLWuExC1dx7iMPdmw+Vxz5tjKSd8D1zCvFwTccEgIfHhT4Qtr2KGnvkO1SEC5U1VYp4Pnz5/H7t27/Zzwsnu6OQTmtawR5kOBoGXqxXOG7FwdiX7mAwQjdr8j1GAwwuYy/ZZaVVrNqpYvp5dEqIoeHd0yGAgiOzbq2UFZUgc3E7iVj1LyOICRBMwbyAYiGAnGeI7uaerad0n1RaYypJOBe6Np6Z9nx/nqo5fH7Yde7vWBfWt3DAmSBKkPwKtoH2cEsGDdTxGveIhSwbTsUEozndzXyNC95N8fXT1qfaCCfPWitOO++s3ZCIZTU0WXU/nwOmWrUuNw3IKdiAyInuFHWMP7m7BIZygIHQXXhlVSiSEj6Jgs5JpFMViTxg6JOeP+R0dGzWu8RKA8yLTF5QGLCZJLVzAqVlxQHARxRYYQvyn7Br+H34zVWfPuLpK6LphFciF1N8eZXdACuqPTqbx92mWcAAb+RiTSYzbQNjGR/yyC9mYZR4yii7xFtUm4BOZAaAlNGP/EBze+/3bF241fRLRYWxgYdf4tNeVwTdlS01RI01ne+Zcy8Yz5vHbYs+vfnmLoVQD/faqX5xZb3HwEuvO4pqtqhEjLoa0Cp4k9ITcP/3Lp07dXmoax67oR3SSJYP/LV3AsptTufFo+j28zC94/jcop+x4QvGBT/0RwzdcTK+yjib3OscR71h+ia3y9/ZgppZHnrC8FfmB1GM9ZR4MZlfYNeUODmtH5D2xa6NUnmpElAAAAAElFTkSuQmCC'

const pycharmIcon = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAhGVYSWZNTQAqAAAACAAFARIAAwAAAAEAAQAAARoABQAAAAEAAABKARsABQAAAAEAAABSASgAAwAAAAEAAgAAh2kABAAAAAEAAABaAAAAAAAAAEgAAAABAAAASAAAAAEAA6ABAAMAAAABAAEAAKACAAQAAAABAAAAGKADAAQAAAABAAAAGAAAAAARDxiuAAAACXBIWXMAAAsTAAALEwEAmpwYAAABWWlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iWE1QIENvcmUgNi4wLjAiPgogICA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPgogICAgICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIgogICAgICAgICAgICB4bWxuczp0aWZmPSJodHRwOi8vbnMuYWRvYmUuY29tL3RpZmYvMS4wLyI+CiAgICAgICAgIDx0aWZmOk9yaWVudGF0aW9uPjE8L3RpZmY6T3JpZW50YXRpb24+CiAgICAgIDwvcmRmOkRlc2NyaXB0aW9uPgogICA8L3JkZjpSREY+CjwveDp4bXBtZXRhPgoZXuEHAAAGtUlEQVRIDW1Wa2wVxxX+Zmb37n35vvy4Bis2kGDUuhiK3YYkMmDSKFJJmyiAU/qnqI2C2qpCqSiKIiW5jVrACYRXiQQ/0opHrIBJ00Soakgc6lQVkewWiu2Ak9h1bWwX29i+ft29uzvTs+M4JVVH2j2zc2a/8/rm7AJ3jg8yRk3bcfPOpfLOAw9WdB66UNF59G93XTtau6DLtGXC/vwDeueNgZ/tP9v/dMp/VgrMlwtj/uHsVqEXGs55vrznkz3Fbl5sA/gPeSS4SnFSSwE558F12Z6DX2t5voGd816+uitSmsr/MVpg1uVm5sq3lb3ar1SGM5aRCwYMZDIcDRkNXNH10hqm5FOurRpEIpRUjoScIVTFpFIcCoYZTsWfbbjy0KaGq998Lhnve9GMRVZPZefg2LiHQPsvkVckv2RALulqXMsYGkm1TiSiUHkP0nZcilapoGlwMMEZBydDE7M579E0W1Ufm3vb8sKYs725eLogxBVKfK//fOmfBgnXn/vDf6CkqZ8adxWt8wbH8/Ynw8z57JYoDAcFAZKKwaHETkkXs4rxQrJVEZ7w+rkDVxqUYidgBRmGPx39EUFdztT/ro/kF1FoA2Qh5I1mAVPA7R0xh39wAsO+5TtGhOYhuj6l6wgwX7P/6vM0fSgGPEYoh3H8eBA7dsz6at8SDVYKzmFQToZoZTXuxhvNZ/Gni+/ivZb3sbdxH2Zo16i/tQB4vel1tLW34e13/oAnvv8EkILhUyj3m1M/LxpUazR4myI2UvgbVMZY+vFLHy/tfUV9tfdQHie3q++s+pbyRzabVYODg3re1NSkCEMNDAzo557eHi27urr0eprynjj1jkq1D3nx1v7Nvi/EWcZ7rlnkk0rCk2SPLNJ2ZzoPx3Xw8v79WLx4Mbq7u1FXV4czZ86grKwMO3fuxLKly7BlyxYcOnRYY01S2pJzU05ScC6jiebAX24/A8aUwU3EaUdcucQsQUWjsjHijV/6RzZtIqClqKysRGtrK6qqqnDz5k0cOXIEGzduxPnz5zV44bJKOD3d6PVCJmzhwZ5CrHjRXtk6XciVEikIHlQUgc4ZARN3dATV1dXYvHkzOjo6sH79eoyPj6O0tFSDtrS0aLn2vvswRuBUXPwi2Yu/V74lngxPulkq8d3cvcU5QxGzDIJVUkNLqjQVPBwM4eTJk4jFYli5cqUGO3bsGJ1kFxMTE7hw4QJGRkbw29de0zrAwuOLz2B14knngaKshclcz/UHEgcImZVwg+ipIF0ljW+YEhNdI7jWcQ19//IpDfiR5HI5NDc3w7Is7N69G7W1tSAC4PTpU58bsJH3YlTBlWx1op9SUHuAMiFR0blv11eGjyp0NNrf69urnnlrq2YFvaUlFZmOG9NzKvCXdAt7Kpcn9fqHl9Z6Sn1bTWS3jz7d+VefuTDojJc6lKAU4a0IzGCciOSPtXX3IjdNXtl5JBIJCCEwNjaGkpISJJNJEIURj8cxOztN0Q3od+b9MBAxB5KvLP9J+UHgthHlMj2kJOrNHDUQhVEZ0Jsvf/iRlv9788Fv3LiBUCiEoaEhrV6y5Iv2Q95J1wgWGI4dXg9cuWII5qWXMxtRlme3CXzSU6jAIjSePYiQFYLnUTOlXhQMBvXpfeH5F5AuLcatf48gEGAw6bwSUeaH37vAiI4etQh3HS0eNuLcTYao104RVx3q+VOUoij1g3RpGpFQeN4AMcv3uLivUAMlUwFq3JRf+k4Q4VBQ4LcmV9ObSkd9nUqicK8a+W6BQR2zepaaKlMO9WcuXM9AJ7pRv65+3qv/c7/edfPz1YWuPC/z9P2gIZycp8yQKHMNp9qYkuYv1az7XCBREHSmc5IvKZE1p7cbBqXkESuLRUaOUiBVgI6kAZvThQCXMCgTJl2+JC/h5CVWVFLX8XzSUc4hLO7J+3X2aj7bVy5cd5+wzG2WRWci77pR5mFXdMhImzMUL1Mm97jFpROk9YCee2LekEcc1J4TuKRzKCnJTAorYLl2vp3VtD1ltteecPzYaq7/up7yt5/HQ2uCczY2IGuvMGcC5RGHpcQsCuMcQjrwSCcIK8CkNLgUJiFTeZkZFJRrvx4Knu1Sd1fndARb6aM/UlzFLtVndDK/fn3Pj4k7L/4jFikKEUUezo7/6kTxR2+OqNTWiHDqTC5r0lEeot8AAtO++f75n9lRcv8iAb8pZpyLLPXepDagtXTzf1naa3foN6puHYvGbk++Si/Iyyue3b6wx5eD449XmKa832LyYaoBGVRXKYImYQdaWOz3Ywt7ldpg/Acxlvd12IxxtQAAAABJRU5ErkJggg=='

const projectDirectory = () => {
	var pathname = window.location.pathname
	var match = pathname.match(/^\/purdue-informatics\/.*\/(.*)\/-\/.*$/)[1]
	var directory = match.split(/-/g).map(word => `${word.substring(0,1).toUpperCase()}${word.substring(1)}`).join('.')
	return directory
}

/*
 * With a fully populated diff table, determine the line number to jump to in the users editor.
 *
 * @param table The table that holds the diff lines
 */
const getLineNumber = table => {
	const lineHolders = table.querySelectorAll('.line_holder')
	const lastDiffLine = lineHolders[lineHolders.length - 1]
	const newLineElement = lastDiffLine.querySelector('.new_line')
	var lineNumber = undefined
	if (newLineElement)
		lineNumber = newLineElement.textContent
	if (!lineNumber) {
		const oldLineElement = lastDiffLine.querySelector('.old_line')
		if (oldLineElement)
			lineNumber = oldLineElement.textContent
	}
	if (!lineNumber) {
		const secondToLastDiffLine = lineHolders[lineHolders.length - 2]
		if (secondToLastDiffLine)
			var lineElement = secondToLastDiffLine.querySelector('.new_line')
		if (!lineElement)
			lineElement = lastDiffLine.querySelector('.old_line')
		lineNumber = lineElement.querySelector('a').getAttribute('data-linenumber')
	}
	return lineNumber ? lineNumber : ''
}

/*
 * Once the button has been added to the DOM and the table fully populated with diff lines, append
 * the actual button to the DOM.
 *
 * @param button The "Copy file path" button
 * @param table The table that holds the diff lines
 */
const appendEditorLink = (button, table) => {
	const clipboardText = button.getAttribute('data-clipboard-text')
	const clipboard = JSON.parse(clipboardText)
	chrome.storage.local.get(['projects', 'editors'], storedValue => {
		const baseUrl = location.href.split("/-/")[0]
		// Don't add link if it's already been added. This gets called multiple times when a diff is
		// expanded and then comments are added. A bit hacky since we don't know what the
		// sibling is. We're assuming it's our button for now
		const nextSibling = button.nextElementSibling
		if (baseUrl !== "" && !nextSibling) {
			const {localPath, projectName} = storedValue.projects[baseUrl]
			// Nothing to link if no local path has been configured yet
			if (!localPath) return
			storedValue.editors.forEach(editor => {
				var url = ''
				if (editor === 'emacs')
					url = `emacs://open?url=file://${localPath}/${clipboard.text}&line=${getLineNumber(table)}`
				if (editor === 'vscode')
					url = `vscode://file/${localPath}/${clipboard.text}:${getLineNumber(table)}`
				if (editor === 'rider')
					// Not sure why, but Rider seems to want to navigate to one line after the line number
					url = `jetbrains://rd/navigate/reference?project=${projectName}&path=${clipboard.text}:${getLineNumber(table)-1}`
				if (editor === 'pycharm')
					// Not sure why, but Pycharm seems to want to navigate to one line after the line number
					url = `jetbrains://pycharm/navigate/reference?project=${projectName}&path=${clipboard.text}:${getLineNumber(table)-1}`
				const link = document.createElement('a')
				link.setAttribute('href', url)
				link.style.marginLeft = "6px"
				const imgNode = document.createElement('img')
				let icon
				switch(editor) {
					case 'emacs':
						icon = emacsIcon
						break
					case 'vscode':
						icon = vscodeIcon
						break
					case 'rider':
						icon = riderIcon
						break;
					case 'pycharm':
						icon = pycharmIcon
						break;
					default:
						icon = emacsIcon
				}
				imgNode.setAttribute('src', icon)
				link.appendChild(imgNode)
				button.parentElement.appendChild(link)
			})
		}
	})
}

/*
 * When the table of diffs is loaded, the actual lines may not be yet. Wait for the lines with the
 * diff content. Then call the function to add the new button to the DOM.
 *
 * @param button The "Copy file path" button
 * @param table The table that holds the diff lines
 */
const waitForLineHolders = (button, table) => {
	const lineHolders = table.querySelectorAll('.line_holder')
	if (lineHolders.length > 1)
		appendEditorLink(button, table)
	else {
		const lineHoldersMutationCallback = lineHoldersMutationsList => {
			// Bit of a hack. Figure if the table has any mutations, it now has line holders
			appendEditorLink(button, table)
		}
		const lineHoldersObserver = new MutationObserver(lineHoldersMutationCallback)
		lineHoldersObserver.observe(table, config)
	}
}

/*
 * When a "Copy file path" button has been found, wait for the accompanying table of diffs to load
 * so we can grab line numbers from it.
 *
 * @param button The "Copy file path" button
 */
const waitForTable = button => {
	const fileHolder = button.parentElement.parentElement.parentElement
	var table = fileHolder.querySelector('table')
	if (!table) {
		const fileHolderMutationCallback = fileHolderMutationsList => {
			fileHolderMutationsList.map(fileHolderMutationRecord => {
				Array.from(fileHolderMutationRecord.addedNodes).forEach(addedNode => {
					table = addedNode.querySelector('table')
					if (table)
						waitForLineHolders(button, table)
				})
			})
		}
		const fileHolderObserver = new MutationObserver(fileHolderMutationCallback)
		fileHolderObserver.observe(fileHolder, config)
	}

	if (table)
		waitForLineHolders(button, table)
}

/*
 * When a "Copy file path" button is added to the DOM, add a button next to it that launches the
 * file in the user's editor.
 *
 * @param mutationsList The list of elements that have mutated in the entire document
 */
const reactToButtonAdd = mutationsList => {
	mutationsList.map(mutationRecord => {
		if (mutationRecord.addedNodes.length)
			Array.from(mutationRecord.addedNodes).forEach(addedNode => {
				// Comment and text nodes don't have querySelector
				if ([3, 8].includes(addedNode.nodeType)) return
				const button = addedNode.querySelector('button[title="Copy file path"]')
				if (button && !addedButonNodes.includes(button)) {
					addedButonNodes.push(button)
					waitForTable(button)
				}
			})
	})
}

// Depending on when this script loads, some buttons may already be rendered.
// Add our links to them
const buttons = document.querySelectorAll('button[title="Copy file path"]')
buttons.forEach(button => {
	waitForTable(button)
})

// For the rest yet to be added to the dom, add our links when they show up.
const addedButonNodes = []
const config = { childList: true, subtree: true }
const observer = new MutationObserver(reactToButtonAdd)
observer.observe(document, config)
